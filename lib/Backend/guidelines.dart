import 'package:flutter/material.dart';

class Guidelines {
  static const double horizontalInset = 15;
  static const double topInset = 10;
  static const Color primaryColor = Color(0xFFD5DDBD);
  static const Color secondaryColor = Color(0xFF333649);
  static const Color tertiaryColor = Color(0xFF226F54);
  static const Color backgroundColor = Color(0xFFFCFEF7);
  static const Color shadowColor = Color.fromRGBO(48, 60, 91, 0.1);
  static const Color placeholderColor = Color(0xFFCCCFC2);
  static TextStyles text = TextStyles();

  static ButtonStyle headerButton = TextButton.styleFrom(
      backgroundColor: backgroundColor,
      foregroundColor: secondaryColor,
      minimumSize: Size.zero,
      padding: const EdgeInsets.all(10),
      elevation: 0,
      shape: const CircleBorder()
  );

  static ButtonStyle headerTextButton = TextButton.styleFrom(
      backgroundColor: backgroundColor,
      foregroundColor: secondaryColor,
      minimumSize: Size.zero,
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 12
      ),
      elevation: 0,
      shape: const StadiumBorder()
  );
}

class TextStyles {
  TextStyle overviewEntryTitle(BuildContext context) => TextStyle(
      color: Theme.of(context).primaryColor,
      fontSize: 17,
      fontFamily: "Outfit",
      height: 1.1,
      fontWeight: FontWeight.w600
  );

  TextStyle overviewEntryDescription(context) => TextStyle(
      color: Theme.of(context).primaryColor.withOpacity(0.75),
      fontSize: 14.5,
      fontFamily: "Outfit",
      fontWeight: FontWeight.w400
  );
}