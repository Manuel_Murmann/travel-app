import 'dart:io';

import 'package:gpx/gpx.dart';
import 'package:permission_handler/permission_handler.dart';

import 'data_management.dart';

class Export {
  static void export(Trip trip, bool asGpx) async {
    PermissionStatus status = await Permission.storage.request();

    Gpx gpx = Gpx();
    gpx.creator = "InstaPin";

    for (Waypoint waypoint in trip.waypoints) {
      gpx.wpts.add(
        Wpt(
          lat: waypoint.position.latitude,
          lon: waypoint.position.longitude,
          time: waypoint.time,
          type: waypoint.label ?? Waypoint.typeToTitle[waypoint.type]
        )
      );
    }

    File file;
    String contents;

    if (asGpx) {
      file = File("/storage/emulated/0/Download/${trip.title}.gpx");
      contents = GpxWriter().asString(gpx, pretty: true);
    } else {
      file = File("/storage/emulated/0/Download/${trip.title}.kml");
      contents = KmlWriter().asString(gpx, pretty: true);
    }

    file.writeAsString(contents);
  }
}