import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

part 'drift_setup.g.dart';

class Trips extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get title => text()();
  DateTimeColumn get start => dateTime()();
  DateTimeColumn get end => dateTime().nullable()();
}

class Waypoints extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get tripId => integer()();

  TextColumn get label => text()();
  DateTimeColumn get arrival => dateTime()();
  DateTimeColumn get departure => dateTime()();
  TextColumn get position => text()();
}

class Details extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get tripId => integer()();

  TextColumn get title => text()();
  TextColumn get content => text()();
}

@DriftDatabase(tables: [Trips, Waypoints, Details])
class InstaPinDatabase extends _$InstaPinDatabase {
  InstaPinDatabase() : super(connectDatabase());

  @override
  int get schemaVersion => 1;
}

LazyDatabase connectDatabase() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase.createInBackground(file);
  });
}