import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:travel_app/Backend/drift_setup.dart' as drift;

import 'data_management.dart';

class DatabaseManagement {
  static final drift.InstaPinDatabase database = drift.InstaPinDatabase();
  
  static Future<int> insertWaypoint(Waypoint waypoint, int tripId) async {
    return database.into(database.waypoints).insert(
      drift.WaypointsCompanion.insert(
        position: jsonEncode(waypoint.position),
        arrival: waypoint.time,
        departure: waypoint.time,
        tripId: tripId,
        label: waypoint.label ?? waypoint.type.toString()
      )
    );
  }

  static void updateWaypoint(Waypoint waypoint) async {
    database.update(database.waypoints)..where((row) {
      return row.id.equals(waypoint.id);
    })..write(drift.WaypointsCompanion(
        position: Value(jsonEncode(waypoint.position)),
        arrival: Value(waypoint.time),
        departure: Value(waypoint.time),
        label: Value(waypoint.label ?? waypoint.type.toString())
    ));
  }

  static void removeWaypoint(Waypoint waypoint) {
    (database.delete(database.waypoints)..where((row) {
      return row.id.equals(waypoint.id);
    })).go();
  }

  static Future<int> insertDetail(TripDetail detail, int tripId) async {
    return database.into(database.details).insert(
        drift.DetailsCompanion.insert(
          title: detail.title,
          content: detail.content,
          tripId: tripId
        )
    );
  }

  static void updateDetail(TripDetail detail) async {
    database.update(database.details)..where((row) {
      return row.id.equals(detail.id);
    })..write(drift.DetailsCompanion(
        title: Value(detail.title),
        content: Value(detail.content)
    ));
  }

  static void removeDetail(TripDetail detail) {
    (database.delete(database.details)..where((row) {
      return row.id.equals(detail.id);
    })).go();
  }

  static Future<int> insertTrip(Trip trip) async {
    return database.into(database.trips).insert(
        drift.TripsCompanion.insert(
          title: trip.title,
          start: trip.start,
          end: Value(trip.end)
        )
    );
  }

  static void updateTrip(Trip trip) async {
    database.update(database.trips)..where((row) {
      return row.id.equals(trip.id);
    })..write(drift.TripsCompanion(
        title: Value(trip.title),
        start: Value(trip.start),
        end: Value(trip.end)
    ));
  }

  static void removeTrip(Trip trip) {
    // TODO: Remove corresponding waypoints and details
    (database.delete(database.trips)..where((row) {
      return row.id.equals(trip.id);
    })).go();
  }

  static Future<List<Trip>> fetchTrips() async {
    List<Trip> tripsArray = [];

    final List<drift.Trip> trips = await database.select(database.trips).get();

    for (int i=0; i<trips.length; i++) {
      Trip trip = Trip.fromDrift(trips[i]);
      List<TripDetail> details = await fetchDetails(trip);
      List<Waypoint> waypoints = await fetchWaypoints(trip);
      trip.details = details;
      trip.waypoints = waypoints;

      tripsArray.add(trip);
    }

    return tripsArray;
  }

  static Future<List<TripDetail>> fetchDetails(Trip trip) async {
    final selection = database.select(database.details)..where((row) => row.tripId.equals(trip.id));
    final List<drift.Detail> details = await selection.get();

    return List.generate(details.length, (index) {
      return TripDetail.fromDrift(details[index]);
    });
  }

  static Future<List<Waypoint>> fetchWaypoints(Trip trip) async {
    final selection = database.select(database.waypoints)..where((row) => row.tripId.equals(trip.id));
    final List<drift.Waypoint> waypoints = await selection.get();

    return List.generate(waypoints.length, (index) {
      return Waypoint.fromDrift(waypoints[index]);
    });
  }
}