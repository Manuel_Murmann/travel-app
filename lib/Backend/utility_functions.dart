import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

import 'data_management.dart';

String parseDate(DateTime dateTime) {
  return "${dateTime.day}.${dateTime.month}.${dateTime.year}";
}

String parseTime(DateTime dateTime, {bool asDuration = false}) {
  if (asDuration) {
    return "${dateTime.hour.toString()}h "
        "${dateTime.minute.toString().padLeft(2, "0")}min";
  }
  return "${dateTime.hour.toString().padLeft(2, "0")}:"
      "${dateTime.minute.toString().padLeft(2, "0")}";
}

String tripLengthToString(Trip trip) {
  if (trip.end == null) {
    if (trip.start.isSameDate(DateTime.now())) {
      return "${parseTime(trip.start)} - Now";
    } else {
      return "${parseDate(trip.start)} - Now";
    }
  }
  if (trip.start.difference(trip.end!).inDays >= 1.0) {
    return "${parseDate(trip.start)} - ${parseDate(trip.end!)}";
  } else {
    return "${parseDate(trip.start)} | ${parseTime(trip.start)} - ${parseTime(trip.end!)}";
  }
}

extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return year == other.year && month == other.month
        && day == other.day;
  }
}

Future<LatLng> determinePosition() async {
  bool locationEnabled = await Geolocator.isLocationServiceEnabled();
  if (!locationEnabled) {
    // TODO: Ask user to enable location
    return Future.error("Location is disabled");
  }

  LocationPermission permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // TODO: Ask user to allow location
      return Future.error("Location denied");
    }
  }

  Position position = await Geolocator.getCurrentPosition();
  return LatLng(position.latitude, position.longitude);
}