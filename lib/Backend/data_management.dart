import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:travel_app/Backend/database_management.dart';
import 'package:travel_app/Backend/drift_setup.dart' as drift;

enum WaypointType {
  beginDay,
  endDay,
  breakStart,
  breakEnd,
  generic,
  custom
}

class Waypoint {
  final LatLng position;
  DateTime time;
  WaypointType type;
  String? label;
  int id = 0;

  Waypoint({
    required this.position,
    required this.time,
    required this.type,
    this.label
  });

  Waypoint.fromDrift(drift.Waypoint waypoint) :
      position = LatLng.fromJson(jsonDecode(waypoint.position)),
      time = waypoint.arrival,
      type = WaypointType.generic,
      id = waypoint.id {
    type = parseLabel(waypoint.label);
    if (type == WaypointType.custom) {
      label = waypoint.label;
    }
  }

  WaypointType parseLabel(String string) {
    WaypointType? type;
    for (WaypointType waypointType in WaypointType.values) {
      if (waypointType.toString() == string) {
        type = waypointType;
      }
    }
    return type ?? WaypointType.custom;
  }

  static Map<WaypointType, String> typeToPath = {
    WaypointType.generic: "marker.svg",
    WaypointType.custom: "marker.svg",
    WaypointType.beginDay: "sunrise-marker.svg",
    WaypointType.endDay: "sunset-marker.svg",
    WaypointType.breakStart: "break-marker.svg",
    WaypointType.breakEnd: "continue-marker.svg",
  };

  static Map<WaypointType, String> typeToTitle = {
    WaypointType.generic: "Standard",
    WaypointType.beginDay: "Begin of day",
    WaypointType.endDay: "End of day",
    WaypointType.breakStart: "Begin of break",
    WaypointType.breakEnd: "End of break",
    WaypointType.custom: "Custom"
  };
}

class TripDetail {
  String title;
  String content;
  int id = 0;
  
  TripDetail({
    this.title = "Tap to edit title",
    this.content = "Tap to edit content"
  });

  TripDetail.fromDrift(drift.Detail detail) :
      title = detail.title,
      content = detail.content,
      id = detail.id;
}

class Trip {
  Trip({
    required this.title,
    required this.start,
  });

  String title;
  int id = 0;
  DateTime start;
  DateTime? end;
  List<Waypoint> waypoints = [];
  List<TripDetail> details = [];

  Trip.fromDrift(drift.Trip trip)
      : id = trip.id,
        title = trip.title,
        start = trip.start,
        end = trip.end;
}

class DetailProvider extends ChangeNotifier {
  TripProvider tripProvider;

  DetailProvider({
    required this.tripProvider
  });

  void update(TripProvider newProvider) {
    tripProvider = newProvider;
  }

  Future addDetail() async {
    TripDetail detail = TripDetail();
    int rowId = await DatabaseManagement.insertDetail(
        detail, tripProvider.selectedTrip!.id
    );
    detail.id = rowId;

    tripProvider.selectedTrip!.details.add(detail);
    notifyListeners();
  }

  void updateDetail(int index, {String? title, String? content}) {
    if (title != null) tripProvider.selectedTrip!.details[index].title = title;
    if (content != null) tripProvider.selectedTrip!.details[index].content = content;

    DatabaseManagement.updateDetail(tripProvider.selectedTrip!.details[index]);
  }

  void removeDetail(int index) {
    TripDetail detail = tripProvider.selectedTrip!.details.removeAt(index);
    DatabaseManagement.removeDetail(detail);
    notifyListeners();
  }
}

class WaypointProvider extends ChangeNotifier {
  TripProvider tripProvider;
  Waypoint? _selectedWaypoint;

  Waypoint? get selectedWaypoint => _selectedWaypoint;
  set selectedWaypoint(Waypoint? waypoint) {
    _selectedWaypoint = waypoint;
    notifyListeners();
  }

  WaypointProvider({
    required this.tripProvider
  });

  void update(TripProvider newProvider) {
    tripProvider = newProvider;
    notifyListeners();
  }

  List<Waypoint>? waypoints() {
    return tripProvider.selectedTrip?.waypoints;
  }

  void addWaypoint(Waypoint waypoint) async {
    int rowId = await DatabaseManagement.insertWaypoint(
        waypoint, tripProvider.activeTrip!.id
    );
    waypoint.id = rowId;
    tripProvider.activeTrip!.waypoints.add(waypoint);
    notifyListeners();
  }

  void updateWaypoint(Waypoint waypoint) {
    if (waypoint.type != WaypointType.custom) {
      waypoint.label = null;
    }
    selectedWaypoint = waypoint;
    DatabaseManagement.updateWaypoint(waypoint);
    notifyListeners();
  }

  void deleteWaypoint(Waypoint waypoint) async {
    waypoints()?.removeWhere((element) => element.id == waypoint.id);
    selectedWaypoint = null;
    DatabaseManagement.removeWaypoint(waypoint);
    notifyListeners();
  }
}

class TripProvider extends ChangeNotifier {
  List<Trip> trips = [];
  Trip? activeTrip;
  Trip? _selectedTrip;

  TripProvider() {
    init();
  }

  set selectedTrip(Trip? trip) {
    _selectedTrip = trip;
    notifyListeners();
  }

  Trip? get selectedTrip => _selectedTrip;

  void init() async {
    trips = await DatabaseManagement.fetchTrips();
    trips.removeWhere((element) {
      if (element.end == null) {
        activeTrip = element;
        return true;
      }
      return false;
    });
    notifyListeners();
  }

  void beginTrip(Trip trip) async {
    int rowId = await DatabaseManagement.insertTrip(trip);
    trip.id = rowId;
    activeTrip = trip;
    selectedTrip = trip;
    notifyListeners();
  }

  void finishTrip() {
    DatabaseManagement.updateTrip(activeTrip!);
    trips.add(activeTrip!);
    activeTrip = null;
    notifyListeners();
  }

  void removeTrip(Trip trip) {
    DatabaseManagement.removeTrip(trip);
    if (activeTrip == trip) {
      activeTrip = null;
    }
    trips.remove(trip);
    notifyListeners();
  }

  void updateTrip(Trip trip) {
    DatabaseManagement.updateTrip(trip);
    selectedTrip = trip;
  }
}

/* Outdated
{
  "trips": [
    {
      "title": "",
      "dateTimeStart": "",
      "dateTimeEnd": "",
      "estimatedLength": 0,
      "waypoints": [
        {
          "position": "",
          "time": "",
          "type": ""
        }
      ]
    }
  ]
}
*/