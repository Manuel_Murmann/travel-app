import 'dart:async';

import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/export.dart';
import 'package:travel_app/Backend/utility_functions.dart';
import 'package:travel_app/Components/Dialogs/confirmation_dialog.dart';
import 'package:travel_app/Components/Dialogs/export_dialog.dart';
import 'package:travel_app/Components/overview_pages.dart';

import 'Backend/guidelines.dart';

enum HandlingMethod {
  edit,
  view,
  finish
}

class Finish extends StatefulWidget {
  const Finish({
    super.key,
    required this.handlingMethod
  });

  final HandlingMethod handlingMethod;

  @override
  State<Finish> createState() => _FinishState();
}

class _FinishState extends State<Finish> {
  late TextEditingController controller;
  late PageController pageController;
  late Completer controllerBound;
  FocusNode titleFocusNode = FocusNode();

  late Trip selectedTrip;

  @override
  void initState() {
    selectedTrip = context.read<TripProvider>().selectedTrip!;

    controller = TextEditingController(text: generateTitle(selectedTrip));
    pageController = PageController(initialPage: 0);
    controllerBound = Completer();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controllerBound.complete();
    });

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    pageController.dispose();
    super.dispose();
  }

  String generateTitle(Trip trip) {
    // TODO: Time based title (or activity based idk)
    return trip.title;
  }

  String generateSubtitle(Trip trip) {
    if (trip.end == null) {
      if (trip.start.isSameDate(DateTime.now())) {
        return "${parseTime(trip.start)} - Now";
      } else {
        return "${parseDate(trip.start)} - Now";
      }
    }
    if (trip.start.difference(trip.end!).inDays.abs() >= 1.0) {
      return "${parseDate(trip.start)} - ${parseDate(trip.end!)}";
    } else {
      return "${parseDate(trip.start)} | ${parseTime(trip.start)} - ${parseTime(trip.end!)}";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Guidelines.primaryColor,
        resizeToAvoidBottomInset: false,
        body: ConstrainedBox(
          constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
          child: Stack(
            children: [
              Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: SvgPicture.asset("assets/trip_overview.svg", fit: BoxFit.cover)
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).viewPadding.top + Guidelines.topInset
                ),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          left: Guidelines.horizontalInset,
                          right: Guidelines.horizontalInset
                        ),
                        child: Row(
                            children: [
                              TextButton(
                                  style: Guidelines.headerButton,
                                  onPressed: () {
                                    context.pop();
                                  },
                                  child: const Icon(
                                      Icons.arrow_back_ios_new_rounded
                                  )
                              ),
                              const Spacer(),
                              TextButton(
                                  style: Guidelines.headerButton,
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return const ConfirmationDialog(
                                          description: "Are you sure you want to delete this trip? It will be gone forever",
                                        );
                                      }
                                    ).then((delete) {
                                      if (delete ?? false) {
                                        context.read<TripProvider>().removeTrip(selectedTrip);
                                        context.goNamed("home");
                                      }
                                    });
                                  },
                                  child: const Icon(
                                      Icons.delete
                                  )
                              ),
                              const SizedBox(width: 5),
                              widget.handlingMethod == HandlingMethod.finish ? TextButton(
                                  style: Guidelines.headerTextButton,
                                  onPressed: () {
                                    context.goNamed("home");

                                    if (widget.handlingMethod == HandlingMethod.finish) {
                                      context.read<TripProvider>().activeTrip?.end ??= DateTime.now();

                                      Provider.of<TripProvider>(
                                          context, listen: false
                                      ).finishTrip();
                                    }
                                  },
                                  child: const Text("Finish", style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600
                                  ))
                              ) : TextButton(
                                  style: Guidelines.headerButton,
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) => const ExportDialog()
                                    );
                                  },
                                  child: const Icon(
                                      Icons.file_open_outlined
                                  )
                              )
                            ]
                        )
                      ),
                      const SizedBox(height: 25),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: Guidelines.horizontalInset,
                            right: Guidelines.horizontalInset
                        ),
                        child: EditableText(
                            focusNode: titleFocusNode,
                            autofocus: widget.handlingMethod == HandlingMethod.finish ? true : false,
                            selectionColor: Guidelines.placeholderColor,
                            showSelectionHandles: true,
                            textInputAction: TextInputAction.done,
                            backgroundCursorColor: Guidelines.backgroundColor,
                            cursorColor: Guidelines.secondaryColor,
                            controller: controller,
                            onSubmitted: (text) {
                              context.read<TripProvider>().updateTrip((
                                  context.read<TripProvider>().selectedTrip!..title = controller.text
                              ));
                              titleFocusNode.unfocus();
                            },
                            onTapOutside: (event) {
                              context.read<TripProvider>().selectedTrip = (
                                  context.read<TripProvider>().selectedTrip!..title = controller.text
                              );
                              titleFocusNode.unfocus();
                            },
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: "Outfit",
                              fontSize: 32,
                              height: 1.2,
                              fontWeight: FontWeight.w800,
                            ),
                            textAlign: TextAlign.center
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: Guidelines.horizontalInset,
                            right: Guidelines.horizontalInset
                        ),
                        child: Text(
                            generateSubtitle(selectedTrip),
                            style: TextStyle(
                              color: Theme.of(context).primaryColor.withOpacity(0.75),
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                            ), textAlign: TextAlign.center
                        ),
                      ),
                      const SizedBox(height: 30),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: Guidelines.horizontalInset,
                            right: Guidelines.horizontalInset
                        ),
                        child: FutureBuilder(
                          future: controllerBound.future,
                          builder: (context, snapshot) {
                            return AnimatedBuilder(
                              animation: pageController,
                              builder: (context, child) {
                                int index = 0;
                                if (snapshot.connectionState == ConnectionState.done) {
                                  index = pageController.page!.round();
                                }

                                List<String> titles = ["Overview", "Details", "Activity"];

                                return Flex(
                                  direction: Axis.horizontal,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    for (int i=0; i<3; i++) TextButton(
                                      onPressed: () {
                                        pageController.animateToPage(
                                          i,
                                          duration: const Duration(milliseconds: 250),
                                          curve: Curves.fastEaseInToSlowEaseOut
                                        );
                                      },
                                      style: TextButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 10),
                                        backgroundColor: index == i ? Guidelines.secondaryColor : Colors.transparent,
                                        foregroundColor: index == i ? Colors.white : Guidelines.secondaryColor
                                      ),
                                      child: Text(titles[i], style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600
                                      )),
                                    )
                                  ]
                                );
                              }
                            );
                          }
                        ),
                      ),
                      const SizedBox(height: 20),
                      Flexible(
                        child: ExpandablePageView(
                          clipBehavior: Clip.none,
                          controller: pageController,
                          animationDuration: const Duration(),
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: Guidelines.horizontalInset
                              ),
                              child: OverviewPage(handlingMethod: widget.handlingMethod),
                            ),
                            const Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: Guidelines.horizontalInset
                              ),
                              child: DetailsPage()
                            ),
                            const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: Guidelines.horizontalInset
                                ),
                                child: ActivityPage()
                            )
                          ]
                        )
                      )
                    ]
                  )
                )
              )
            ]
          )
        )
    );
  }
}