import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Components/bounded_map.dart';
import 'package:travel_app/finish.dart';

import '../Backend/utility_functions.dart';

class ActiveTrip extends StatelessWidget {
  const ActiveTrip({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10,
        bottom: 20,
        left: Guidelines.horizontalInset,
        right: Guidelines.horizontalInset,
      ),
      child: Consumer2<TripProvider, WaypointProvider>(
          builder: (context, tripProvider, waypointProvider, child) {
            return GestureDetector(
                onTap: () {
                  context.read<TripProvider>().selectedTrip =
                      context.read<TripProvider>().activeTrip;
                  context.pushNamed("navigation", extra: HandlingMethod.edit);
                },
                child: Container(
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15)
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: Stack(
                    children: [
                      BoundedMap(trip: tripProvider.activeTrip!),
                      const Positioned(
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        child: DecoratedBox(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Colors.transparent,
                                      Color.fromRGBO(0, 0, 0, 0.6)
                                    ]
                                )
                            )
                        ),
                      ),
                      Positioned(
                        bottom: 15,
                        left: 15,
                        right: 15,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(tripProvider.activeTrip!.title, style: const TextStyle(
                              fontSize: 19,
                              color: Colors.white,
                              fontWeight: FontWeight.w600
                            )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  tripLengthToString(tripProvider.activeTrip!),
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.white.withOpacity(0.75),
                                    fontWeight: FontWeight.w500
                                  )
                                ),
                                Text(
                                  "${tripProvider.activeTrip!.waypoints.length.toString()} Waypoints",
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.white.withOpacity(0.75),
                                    fontWeight: FontWeight.w500
                                  )
                                )
                              ]
                            )
                          ]
                        )
                      )
                    ]
                  )
                )
            );
          }
      )
    );
  }
}