import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Components/trip_entry.dart';
import 'package:travel_app/finish.dart';

enum RecentTripsSelector {
  all,
  favourites,
  longest
}

class RecentTrips extends StatelessWidget {
  const RecentTrips({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10,
        left: Guidelines.horizontalInset,
        right: Guidelines.horizontalInset
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Recent Trips", style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.w600
            )),
            Consumer<TripProvider>(
                builder: (context, tripProvider, child) {
                  return Column(
                      children: [
                        for (Trip trip in tripProvider.trips) Padding(
                          padding: const EdgeInsets.only(
                              top: 10
                          ),
                          child: GestureDetector(
                            onTap: () {
                              context.read<TripProvider>().selectedTrip = trip;
                              context.pushNamed("finish", extra: HandlingMethod.edit);
                            },
                            child: TripEntry(trip: trip)
                          )
                        )
                      ]
                  );
                }
            )
          ]
      ),
    );
  }
}