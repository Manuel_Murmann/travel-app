import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/svg.dart';
import 'package:latlong2/latlong.dart';

import '../Backend/data_management.dart';

class BoundedMap extends StatelessWidget {
  const BoundedMap({
    super.key,
    required this.trip,
    this.onTap
  });

  final Trip trip;
  final Function(TapPosition, LatLng)? onTap;

  LatLngBounds? calculateBounds(Trip trip) {
    List<LatLng> positionList = [];

    for (Waypoint waypoint in trip.waypoints) {
      positionList.add(waypoint.position);
    }

    if (positionList.isEmpty) return null;
    return LatLngBounds.fromPoints(positionList);
  }

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
        options: MapOptions(
            bounds: calculateBounds(trip),
            center: LatLng(50, 10),
            onTap: onTap,
            boundsOptions: const FitBoundsOptions(
                padding: EdgeInsets.all(30)
            ),
            zoom: 3,
            maxZoom: 17.0,
            interactiveFlags: InteractiveFlag.none
        ),
        children: [
          TileLayer(
            urlTemplate: "https://api.mapbox.com/styles/v1/manuelmurmann/cll3zfuzz002m01qpfsnwa30y/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibWFudWVsbXVybWFubiIsImEiOiJjbGwzejdxbWMwMWdtM2RybnJ3ODhwdTBuIn0.nvPwnha7q-7fJVM0VFa0yw",
            userAgentPackageName: 'com.example.app',
          ),
          MarkerLayer(
              rotateAlignment: Alignment.bottomCenter,
              markers: [
                for (Waypoint waypoint in trip.waypoints) Marker(
                    point: waypoint.position,
                    width: 40,
                    rotate: true,
                    height: 40,
                    builder: (context) => SvgPicture.asset(
                        "assets/${Waypoint.typeToPath[waypoint.type]}",
                        width: 40
                    )
                )
              ]
          )
        ]
    );
  }
}
