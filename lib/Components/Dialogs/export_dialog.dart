import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/export.dart';
import 'package:travel_app/Components/Dialogs/dialog_scaffold.dart';

import '../../Backend/guidelines.dart';

class ExportDialog extends StatelessWidget {
  const ExportDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return DialogScaffold(
      title: "Export this trip",
      actions: [
        TextButton(
          style: DialogScaffold.actionStyle,
          onPressed: () {
            Export.export(context.read<TripProvider>().selectedTrip!, true);
          },
          child: const Text("Export GPX")
        ),
        TextButton(
          style: DialogScaffold.actionStyle,
          onPressed: () {
            Export.export(context.read<TripProvider>().selectedTrip!, false);
          },
          child: const Text("Export KML")
        )
      ],
      children: [
        Text(
          "You can either export the waypoints as GPX (most widespread format) or KML. The file will be saved to your downloads folder",
          style: TextStyle(
              color: Guidelines.secondaryColor.withOpacity(0.75)
          )
        )
      ]
    );
  }
}
