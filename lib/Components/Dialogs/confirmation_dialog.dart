import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Components/Dialogs/dialog_scaffold.dart';

class ConfirmationDialog extends StatelessWidget {
  const ConfirmationDialog({
    super.key,
    required this.description
  });

  final String description;

  @override
  Widget build(BuildContext context) {
    return DialogScaffold(
      title: "Are you sure?",
      actions: [
        TextButton(
          style: DialogScaffold.actionStyle,
          onPressed: () => context.pop(true),
          child: const Text("Delete"),
        ),
        TextButton(
          style: DialogScaffold.actionStyle,
          onPressed: () => context.pop(false),
          child: const Text("Go Back"),
        )
      ],
      children: [
        Text(
          description,
          style: TextStyle(
            color: Guidelines.secondaryColor.withOpacity(0.75)
          )
        )
      ],
    );
  }
}
