import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:travel_app/Components/Dialogs/dialog_scaffold.dart';

import '../../Backend/data_management.dart';
import '../../Backend/guidelines.dart';

class WaypointTypeDialog extends StatelessWidget {
  const WaypointTypeDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return DialogScaffold(
      title: "Select Waypoint",
      children: [
        for (WaypointType type in WaypointType.values) WaypointButton(
          type: type,
          onPressed: () => context.pop(type),
        )
      ],
    );
  }
}

class WaypointButton extends StatelessWidget {
  const WaypointButton({
    super.key,
    required this.type,
    required this.onPressed
  });

  final WaypointType type;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Guidelines.secondaryColor,
          minimumSize: Size.zero,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
        onPressed: onPressed,
        child: Row(
            children: [
              SvgPicture.asset(
                  "assets/${Waypoint.typeToPath[type]}",
                  width: 30,
                  height: 30
              ),
              const SizedBox(width: 10),
              Text(Waypoint.typeToTitle[type]!, style: TextStyle(
                  fontSize: 16,
                  color: Guidelines.secondaryColor.withOpacity(0.75),
                  fontWeight: FontWeight.w600
              ))
            ]
        )
    );
  }
}
