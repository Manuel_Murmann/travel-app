import 'package:flutter/material.dart';

import '../../Backend/guidelines.dart';

class DialogScaffold extends StatelessWidget {
  const DialogScaffold({
    super.key,
    required this.title,
    required this.children,
    this.actions
  });

  final String title;
  final List<Widget> children;
  final List<Widget>? actions;

  static ButtonStyle actionStyle = TextButton.styleFrom(
    backgroundColor: Guidelines.primaryColor,
    foregroundColor: Guidelines.secondaryColor
  );

  List<Widget>? wrapWithAlignment() {
    return actions?.map((button) {
      return Align(
        alignment: Alignment.bottomRight,
        child: button
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Guidelines.backgroundColor,
            borderRadius: BorderRadius.circular(15)
          ),
          constraints: const BoxConstraints(maxWidth: 300),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 20,
                    fontWeight: FontWeight.w600
                )),
                const SizedBox(height: 12),
                ...children,
                if (actions != null) const SizedBox(height: 10),
                ...?wrapWithAlignment()
              ]
          )
        )
      )
    );
  }
}
