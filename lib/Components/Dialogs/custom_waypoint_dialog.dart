import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Components/Dialogs/dialog_scaffold.dart';

class CustomWaypointDialog extends StatefulWidget {
  const CustomWaypointDialog({super.key});

  @override
  State<CustomWaypointDialog> createState() => _CustomWaypointDialogState();
}

class _CustomWaypointDialogState extends State<CustomWaypointDialog> {
  final TextEditingController controller = TextEditingController(text: "Waypoint Title");

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DialogScaffold(
      title: "Custom Waypoint",
      actions: [
        TextButton(
          onPressed: () => context.pop(controller.text),
          style: DialogScaffold.actionStyle,
          child: const Text("Use Waypoint"),
        )
      ],
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10
          ),
          child: Flex(
            direction: Axis.horizontal,
            children: [
              Flexible(
                child: SvgPicture.asset(
                    "assets/marker.svg",
                    width: 30,
                    height: 30
                )
              ),
              const SizedBox(width: 10),
              Flexible(
                child: EditableText(
                    focusNode: FocusNode(),
                    controller: controller,
                    selectionColor: Guidelines.placeholderColor,
                    textInputAction: TextInputAction.done,
                    backgroundCursorColor: Guidelines.backgroundColor,
                    cursorColor: Guidelines.secondaryColor,
                    style: TextStyle(
                        fontSize: 16,
                        color: Guidelines.secondaryColor,
                        fontFamily: "Outfit",
                        fontWeight: FontWeight.w600
                    )
                ),
              )
            ]
          ),
        )
      ]
    );
  }
}
