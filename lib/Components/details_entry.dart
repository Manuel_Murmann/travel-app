import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/guidelines.dart';

class OverviewEntry extends StatelessWidget {
  const OverviewEntry({super.key,
    required this.title,
    this.description,
    this.action,
    this.actionTitle
  });

  final Widget title;
  final Widget? description;
  final Function()? action;
  final String? actionTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 15,
        bottom: description == null ? 10 : 15,
        right: 15,
        top: action == null ? 15 : 10
      ),
      margin: const EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: DefaultTextStyle(
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 17,
                      fontFamily: "Outfit",
                      height: 1.1,
                      fontWeight: FontWeight.w600
                  ),
                  child: title
                ),
              ),
              if (actionTitle != null) TextButton(
                onPressed: action,
                style: TextButton.styleFrom(
                  backgroundColor: Guidelines.primaryColor,
                  foregroundColor: Guidelines.secondaryColor,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 8
                  ),
                  minimumSize: Size.zero
                ),
                child: Text(actionTitle!)
              )
            ]
          ),
          if (description != null) const SizedBox(height: 10),
          if (description != null) DefaultTextStyle(
            style: TextStyle(
                color: Theme.of(context).primaryColor.withOpacity(0.75),
                fontSize: 14.5,
                fontFamily: "Outfit",
                fontWeight: FontWeight.w400
            ),
            child: description!
          )
        ]
      )
    );
  }
}


class DetailsEntry extends StatefulWidget {
  const DetailsEntry({
    super.key,
    required this.index
  });
  
  final int index;

  @override
  State<DetailsEntry> createState() => _DetailsEntryState();
}

class _DetailsEntryState extends State<DetailsEntry> {
  late TextEditingController titleController;
  late TextEditingController contentController;

  @override
  void initState() {
    TripDetail detail = context.read<TripProvider>().selectedTrip!.details[widget.index];
    titleController = TextEditingController(text: detail.title);
    contentController = TextEditingController(text: detail.content);
    super.initState();
  }

  @override
  void dispose() {
    titleController.dispose();
    contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OverviewEntry(
      title: EditableText(
        style: Guidelines.text.overviewEntryTitle(context),
        controller: titleController,
        onSubmitted: (text) {
          if (text == "") {
            context.read<DetailProvider>().removeDetail(
              widget.index
            );
          } else {
            context.read<DetailProvider>().updateDetail(
                widget.index, title: text
            );
          }
        },
        onTapOutside: (event) {
          context.read<DetailProvider>().updateDetail(
              widget.index, title: titleController.text
          );
        },
        focusNode: FocusNode(),
        cursorColor: Guidelines.secondaryColor,
        backgroundCursorColor: Guidelines.backgroundColor
      ),
      description: EditableText(
        maxLines: null,
        controller: contentController,
        style: Guidelines.text.overviewEntryDescription(context),
        onTapOutside: (event) {
          context.read<DetailProvider>().updateDetail(
              widget.index, content: contentController.text
          );
        },
        focusNode: FocusNode(),
        cursorColor: Guidelines.secondaryColor,
        backgroundCursorColor: Guidelines.backgroundColor,
      )
    );
  }
}
