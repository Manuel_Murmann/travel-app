import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
// import 'package:health/health.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/utility_functions.dart';
import 'package:travel_app/Components/bounded_map.dart';
import 'package:travel_app/finish.dart';

import '../Backend/data_management.dart';
import 'details_entry.dart';

class ActivityPage extends StatefulWidget {
  const ActivityPage({super.key});

  @override
  State<ActivityPage> createState() => _ActivityPageState();
}

class _ActivityPageState extends State<ActivityPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        OverviewEntry(
          title: const Text("Connect Google Fit"),
          description: const Text("Include activity data like steps and burnt calories in your trip"),
          actionTitle: "Link",
          action: () async {
            // TODO: Google Fit connection
            /*HealthFactory health = HealthFactory(useHealthConnectIfAvailable: true);

            List<HealthDataType> types = [
              HealthDataType.WORKOUT
            ];

            bool requested = await health.requestAuthorization(types);*/
          }
        )
      ]
    );
  }
}


class DetailsPage extends StatefulWidget {
  const DetailsPage({super.key});

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  List<DetailsEntry> fetchEntries() {
    List<DetailsEntry> entries = [];

    int numDetails = context.watch<TripProvider>().selectedTrip!.details.length;
    for (int i=0; i<numDetails; i++) {
      entries.add(DetailsEntry(
        key: UniqueKey(),
        index: i
      ));
    }

    return entries;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<DetailProvider>(
      builder: (context, detailProvider, child) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            ...fetchEntries(),
            OverviewEntry(
              title: const Text("Add Details"),
              description: const Text("Clear the title and press enter to delete a detail"),
              actionTitle: "Add",
              action: () {
                detailProvider.addDetail();
              }
            ),
            const SizedBox(height: 15),
          ]
        );
      }
    );
  }
}


class OverviewPage extends StatelessWidget {
  const OverviewPage({
    super.key,
    required this.handlingMethod,
  });

  final HandlingMethod handlingMethod;

  LatLngBounds? calculateBounds(Trip trip) {
    List<LatLng> positionList = [];

    for (Waypoint waypoint in trip.waypoints) {
      positionList.add(waypoint.position);
    }

    if (positionList.isEmpty) return null;
    return LatLngBounds.fromPoints(positionList);
  }

  String calcAvgDayLength(Trip trip) {
    // TODO: Wrong calculation
    List<Waypoint> waypoints = trip.waypoints;

    int tripLength = trip.start.difference(trip.end ?? DateTime.now()).inDays;

    DateTime begin = DateTime(1900);
    Duration difference = const Duration();

    for (Waypoint waypoint in waypoints) {
      if (waypoint.type == WaypointType.beginDay) {
        begin = waypoint.time;
      } else if (waypoint.type == WaypointType.endDay && begin.year != 1900) {
        difference += begin.difference(waypoint.time);
      }
    }

    double avgLength = difference.inMinutes.abs() / max(1, tripLength);
    int hours = (avgLength / 60).round();
    int minutes = (avgLength % 60).round();

    DateTime dateTime = DateTime.now().copyWith(hour: hours, minute: minutes);

    return parseTime(dateTime, asDuration: true);
  }

  @override
  Widget build(BuildContext context) {
    Trip selectedTrip = context.read<TripProvider>().selectedTrip!;

    return Column(
      children: [
        OverviewEntry(
          title: const Text("Your Path"),
          description: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  height: 250,
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: BoundedMap(
                    trip: selectedTrip,
                    onTap: (_, __) {
                      if (handlingMethod == HandlingMethod.finish) return;
                      context.pushNamed("navigation", extra: HandlingMethod.view);
                    },
                  )
              )
            ]
          )
        ),
        OverviewEntry(
          title: const Text("Statistics"),
          description: Text(
            "Number of waypoints: ${selectedTrip.waypoints.length}\n"
            "Average day length: ${calcAvgDayLength(selectedTrip)}"
          )
        )
      ]
    );
  }
}
