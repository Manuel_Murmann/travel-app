import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Components/Dialogs/confirmation_dialog.dart';
import 'package:travel_app/Components/Dialogs/waypoint_type_dialog.dart';

import '../../Backend/guidelines.dart';
import '../../Backend/utility_functions.dart';
import '../Dialogs/custom_waypoint_dialog.dart';

class WaypointDetails extends StatelessWidget {
  const WaypointDetails({super.key});

  String getTitle(Waypoint waypoint) {
    if (waypoint.type == WaypointType.custom) {
      return waypoint.label ?? "Custom";
    } else {
      return Waypoint.typeToTitle[waypoint.type]!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<WaypointProvider>(
      builder: (context, provider, child) {
        if (provider.selectedWaypoint == null) return const SizedBox();
        return Container(
            decoration: BoxDecoration(
                color: Guidelines.backgroundColor,
                borderRadius: BorderRadius.circular(15),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromRGBO(48, 91, 68, 0.15),
                      blurRadius: 13
                  )
                ]
            ),
            padding: const EdgeInsets.all(7),
            margin: const EdgeInsets.only(
                left: Guidelines.horizontalInset,
                right: Guidelines.horizontalInset,
                bottom: 10
            ),
            child: Row(
                children: [
                  TextButton(
                      onPressed: () async {
                        WaypointType? type = await showDialog(
                          context: context,
                          builder: (context) {
                            return const WaypointTypeDialog();
                          }
                        );
                        if (type == null) return;

                        if (type == WaypointType.custom) {
                          String? customWaypoint;

                          customWaypoint = await showDialog(
                              context: context,
                              builder: (context) {
                                return const CustomWaypointDialog();
                              }
                          );

                          if (customWaypoint == null) return;

                          Waypoint waypoint = provider.selectedWaypoint!;
                          waypoint.type = WaypointType.custom;
                          waypoint.label = customWaypoint;

                          provider.updateWaypoint(waypoint);
                        } else {
                          provider.updateWaypoint(
                              provider.selectedWaypoint!..type = type
                          );
                        }
                      },
                      style: TextButton.styleFrom(
                          minimumSize: Size.zero,
                          fixedSize: const Size(50, 50),
                          padding: EdgeInsets.zero,
                          foregroundColor: Guidelines.secondaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)
                          )
                      ),
                      child: SvgPicture.asset(
                          "assets/${Waypoint.typeToPath[provider.selectedWaypoint!.type]}",
                          width: 23
                      )
                  ),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            getTitle(provider.selectedWaypoint!),
                            style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Guidelines.secondaryColor
                            )
                        ),
                        Text(
                            "${parseTime(provider.selectedWaypoint!.time)} | "
                                "${parseDate(provider.selectedWaypoint!.time)}",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Guidelines.secondaryColor.withOpacity(0.75)
                            )
                        )
                      ]
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () async {
                      bool delete = await showDialog(
                        context: context,
                        builder: (context) {
                          return const ConfirmationDialog(
                            description: "You're about to delete a waypoint. This action cannot be undone."
                          );
                        }
                      );

                      if (delete) {
                        provider.deleteWaypoint(provider.selectedWaypoint!);
                      }
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: const Color(0xFFB02E2E),
                      fixedSize: const Size(45, 45),
                      minimumSize: Size.zero,
                      padding: EdgeInsets.zero,
                    ),
                    child: const Icon(Icons.delete),
                  )
                ]
            )
        );
      }
    );
  }
}
