import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Backend/utility_functions.dart';
import 'package:travel_app/Components/Dialogs/waypoint_type_dialog.dart';

import '../../finish.dart';
import '../Dialogs/custom_waypoint_dialog.dart';

class NavigatorBottombar extends StatelessWidget {
  const NavigatorBottombar({
    super.key,
    required this.controller
  });

  final MapController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Guidelines.backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(48, 91, 68, 0.15),
            blurRadius: 13
          )
        ],
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25)
        )
      ),
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Consumer<TripProvider>(
            builder: (context, tripProvider, child) {
              return Text(tripProvider.selectedTrip!.title, style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 20,
                  fontWeight: FontWeight.w600
              ));
            }
          ),
          const SizedBox(height: 10),
          Flex(
            direction: Axis.horizontal,
            children: [
              Flexible(
                flex: 3,
                fit: FlexFit.tight,
                child: TextButton(
                  style: TextButton.styleFrom(
                      minimumSize: Size.zero,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 25,
                          vertical: 15
                      ),
                      backgroundColor: Guidelines.primaryColor,
                      foregroundColor: Guidelines.secondaryColor,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))
                  ),
                  onPressed: () async {
                    Future<LatLng> position = determinePosition();

                    showDialog(
                      context: context,
                      builder: (context) {
                        return const WaypointTypeDialog();
                      }
                    ).then((waypointType) async {
                      if (waypointType == null) return;

                      String? customWaypoint;

                      if (waypointType == WaypointType.custom) {
                        customWaypoint = await showDialog(
                          context: context,
                          builder: (context) {
                            return const CustomWaypointDialog();
                          }
                        );
                        if (customWaypoint == null) return;
                      }

                      LatLng loadedPosition = await position;

                      context.read<WaypointProvider>().addWaypoint(Waypoint(
                        position: loadedPosition,
                        time: DateTime.now(),
                        type: waypointType,
                        label: customWaypoint
                      ));
                      controller.moveAndRotate(loadedPosition, 15, 0);
                    });
                  },
                  child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SvgPicture.asset(
                            "assets/marker.svg",
                            width: 15,
                            colorFilter: const ColorFilter.mode(Guidelines.secondaryColor, BlendMode.srcIn)
                        ),
                        const SizedBox(width: 10),
                        const Text("Add Waypoint", style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600
                        ))
                      ]
                  )
                )
              ),
              const SizedBox(width: 15),
              Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: TextButton(
                    style: TextButton.styleFrom(
                        minimumSize: Size.zero,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: 15
                        ),
                        backgroundColor: Theme.of(context).primaryColor.withOpacity(0.1),
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100))
                    ),
                    onPressed: () {
                      context.read<TripProvider>().selectedTrip = context.read<TripProvider>().activeTrip!;
                      context.pushNamed("finish", extra: HandlingMethod.finish);
                    },
                    child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text("Finish", style: TextStyle(
                              fontSize: 16,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w600
                          ))
                        ]
                    )
                )
              )
            ]
          )
        ]
      )
    );
  }
}