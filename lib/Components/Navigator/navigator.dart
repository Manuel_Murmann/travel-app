import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Backend/utility_functions.dart';
import 'package:travel_app/Components/Navigator/navigator_appbar.dart';
import 'package:travel_app/Components/Navigator/navigator_bottombar.dart';
import 'package:travel_app/Components/Navigator/waypoint_details.dart';
import 'package:travel_app/finish.dart';

class TravelNavigator extends StatefulWidget {
  const TravelNavigator({
    super.key,
    required this.handlingMethod
  });

  final HandlingMethod handlingMethod;

  @override
  State<TravelNavigator> createState() => _TravelNavigatorState();
}

class _TravelNavigatorState extends State<TravelNavigator> {
  MapController controller = MapController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  LatLng getCenter() {
    LatLng center = LatLng(50, 10);
    center = context.read<
        WaypointProvider
    >().waypoints()?.firstOrNull?.position ?? center;
    return center;
  }

  double getZoom() {
    double zoom = 3;
    zoom = (context.read<
        WaypointProvider
    >().waypoints() ?? []).isEmpty ? zoom : 17.0;
    return zoom;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          FlutterMap(
            mapController: controller,
            options: MapOptions(
              center: getCenter(),
              zoom: getZoom(),
              minZoom: 2.0,
              maxZoom: 17.0,
              onTap: (position, latlng) {
                setState(() {
                  context.read<WaypointProvider>().selectedWaypoint = null;
                });
              }
            ),
            children: [
              TileLayer(
                urlTemplate: "https://api.mapbox.com/styles/v1/manuelmurmann/cll3zfuzz002m01qpfsnwa30y/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibWFudWVsbXVybWFubiIsImEiOiJjbGwzejdxbWMwMWdtM2RybnJ3ODhwdTBuIn0.nvPwnha7q-7fJVM0VFa0yw",
                userAgentPackageName: 'com.example.app',
              ),
              Consumer<WaypointProvider>(
                builder: (context, waypointProvider, child) {
                  return MarkerLayer(
                    rotateAlignment: Alignment.bottomCenter,
                      markers: [
                        for (Waypoint waypoint in (waypointProvider.waypoints() ?? [])) Marker(
                            point: waypoint.position,
                            width: 40,
                            rotate: true,
                            height: 40,
                            builder: (context) => GestureDetector(
                              onTap: () {
                                context.read<WaypointProvider>().selectedWaypoint = waypoint;
                              },
                              child: SvgPicture.asset("assets/${Waypoint.typeToPath[waypoint.type]}", width: 40)
                            )
                        )
                      ]
                  );
                }
              )
            ]
          ),
          const NavigatorAppbar(),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const WaypointDetails(),
                if (widget.handlingMethod != HandlingMethod.view) NavigatorBottombar(controller: controller)
              ]
            )
          )
        ]
      )
    );
  }
}
