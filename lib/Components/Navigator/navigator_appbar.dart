import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:travel_app/Backend/guidelines.dart';

class NavigatorAppbar extends StatelessWidget {
  const NavigatorAppbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).viewPadding.top + Guidelines.topInset,
        left: Guidelines.horizontalInset,
        right: Guidelines.horizontalInset
      ),
      child: Row(
          children: [
            TextButton(
              style: Guidelines.headerButton,
              onPressed: () {
                context.pop();
              },
              child: const Icon(
                  Icons.arrow_back_ios_new_rounded
              )
            )
          ]
      )
    );
  }
}
