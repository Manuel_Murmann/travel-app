import 'package:flutter/material.dart';

import '../Backend/data_management.dart';
import '../Backend/utility_functions.dart';

class TripEntry extends StatelessWidget {
  const TripEntry({
    super.key,
    required this.trip
  });

  final Trip trip;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10)
        ),
        margin: const EdgeInsets.only(right: 15),
        padding: const EdgeInsets.symmetric(
          horizontal: 10
        ),
        child: Row(
            children: [
              Container(
                  width: 45,
                  height: 45,
                  margin: const EdgeInsets.only(right: 10),
                  decoration: BoxDecoration(
                    image: const DecorationImage(
                      image: AssetImage("assets/cover.jpg"), // TODO: Better cover
                      fit: BoxFit.fill
                    ),
                    color: const Color(0xFFCCCFC2),
                    borderRadius: BorderRadius.circular(5)
                  )
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(trip.title, style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 16,
                        height: 1.0,
                        fontWeight: FontWeight.w600
                    )),
                    Text(tripLengthToString(trip), style: TextStyle(
                        color: Theme.of(context).primaryColor.withOpacity(0.55),
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ))
                  ]
              )
            ]
        )
    );
  }
}
