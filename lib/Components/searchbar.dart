import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:travel_app/Backend/guidelines.dart';

class Searchbar extends StatelessWidget {
  const Searchbar({
    super.key,
    required this.controller
  });

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Guidelines.primaryColor,
        borderRadius: BorderRadius.circular(100)
      ),
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: [
          TextField(
            focusNode: FocusNode(),
            cursorColor: Guidelines.secondaryColor,
            style: const TextStyle(
                color: Guidelines.secondaryColor,
                fontSize: 17,
                fontWeight: FontWeight.w500
            ),
            decoration: const InputDecoration(
                hintText: "Search Trip",
                hintStyle: TextStyle(
                    color: Guidelines.secondaryColor,
                    fontSize: 17,
                    fontWeight: FontWeight.w500
                ),
                suffixIcon: Padding(
                    padding: EdgeInsets.only(right: 15),
                    child: Icon(Icons.search, size: 28)
                ),
                contentPadding: EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: 17
                ),
                border: InputBorder.none
            ),
            controller: controller
          ),
          Positioned(
            bottom: 0,
            right: 60,
            child: SvgPicture.asset("assets/mountain.svg", width: 125)
          )
        ]
      )
    );
  }
}
