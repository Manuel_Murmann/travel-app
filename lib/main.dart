import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/export.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Components/Navigator/navigator.dart';

import 'finish.dart';
import 'home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Cache the finish page svg because it takes so long to load
  const loader = SvgAssetLoader('assets/trip_overview.svg');
  svg.cache.putIfAbsent(loader.cacheKey(null), () => loader.loadBytes(null));

  runApp(const TravelApp());
}

final GoRouter router = GoRouter(
    routes: [
      GoRoute(
        path: '/',
        name: "home",
        builder: (context, state) => const Home(),
      ),
      GoRoute(
        path: '/navigation',
        name: "navigation",
        builder: (context, state) {
          return TravelNavigator(
            handlingMethod: state.extra as HandlingMethod,
          );
        }
      ),
      GoRoute(
          path: '/navigation/finish',
          name: "finish",
          builder: (context, state) {
            return Finish(
              handlingMethod: state.extra as HandlingMethod
            );
          }
      )
    ]
);

class TravelApp extends StatelessWidget {
  const TravelApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => TripProvider(),
      child: MultiProvider(
        providers: [
          ChangeNotifierProxyProvider<TripProvider, DetailProvider>(
            create: (context) {
              return DetailProvider(
                  tripProvider: context.read<TripProvider>()
              );
            },
            lazy: false,
            update: (context, tripProvider, detailProvider) {
              return detailProvider!..update(tripProvider);
            }
          ),
          ChangeNotifierProxyProvider<TripProvider, WaypointProvider>(
              create: (context) {
                return WaypointProvider(
                    tripProvider: context.read<TripProvider>()
                );
              },
              lazy: false,
              update: (context, tripProvider, waypointProvider) {
                return waypointProvider!..update(tripProvider);
              }
          )
        ],
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: MaterialApp.router(
              theme: ThemeData(
                useMaterial3: true,
                scaffoldBackgroundColor: Guidelines.backgroundColor,
                fontFamily: "Outfit",
                primaryColor: const Color(0xFF333649)
              ),
              routerConfig: router
          )
        ),
      )
    );
  }
}
