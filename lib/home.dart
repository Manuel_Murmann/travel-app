import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:travel_app/Backend/data_management.dart';
import 'package:travel_app/Backend/guidelines.dart';
import 'package:travel_app/Components/Dialogs/dialog_scaffold.dart';
import 'package:travel_app/Components/active_trip.dart';
import 'package:travel_app/Components/recent_trips.dart';
import 'package:travel_app/Components/searchbar.dart';
import 'package:travel_app/finish.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    RecentTripsSelector selector = RecentTripsSelector.all;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).viewPadding.top + Guidelines.topInset,
                left: Guidelines.horizontalInset,
                right: Guidelines.horizontalInset
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                              style: Guidelines.headerButton,
                              onPressed: () {
                                // TODO: Settings
                                print("Open Settings");
                              },
                              child: const Icon(
                                Icons.settings_outlined,
                                size: 30
                              )
                          ),
                          TextButton(
                              style: Guidelines.headerButton,
                              onPressed: () {
                                // TODO: Export
                                print("Export Data");
                              },
                              child: const Icon(
                                Icons.file_open_outlined,
                                size: 30
                              )
                          )
                        ]
                    ),
                    const SizedBox(height: 25),
                    Text("Ready For Your Next Adventure?", style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 32,
                        height: 1.2,
                        fontWeight: FontWeight.w800,
                    ), textAlign: TextAlign.center),
                    const SizedBox(height: 35),
                    Searchbar(controller: TextEditingController()) // TODO: Implement search
                  ]
              )
            ),
            const SizedBox(height: 10),
            Consumer<TripProvider>(
              child: const ActiveTrip(),
              builder: (context, tripProvider, child) {
                if (tripProvider.activeTrip != null) {
                  return child!;
                } else {
                  return const SizedBox();
                }
              }
            ),
            Consumer<TripProvider>(
                child: StatefulBuilder(
                  builder: (context, setBuilderState) {
                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                              onPressed: () {
                                setBuilderState(() {
                                  selector = RecentTripsSelector.all;
                                });
                              },
                              style: TextButton.styleFrom(
                                backgroundColor: selector == RecentTripsSelector.all
                                    ? Guidelines.tertiaryColor : Guidelines.primaryColor,
                                foregroundColor: selector == RecentTripsSelector.all
                                    ? Colors.white : Guidelines.secondaryColor,
                              ),
                              child: const Text("All"),
                            ),
                            const SizedBox(width: 10),
                            TextButton(
                              onPressed: () {
                                setBuilderState(() {
                                  selector = RecentTripsSelector.favourites;
                                });
                              },
                              style: TextButton.styleFrom(
                                backgroundColor: selector == RecentTripsSelector.favourites
                                    ? Guidelines.tertiaryColor : Guidelines.primaryColor,
                                foregroundColor: selector == RecentTripsSelector.favourites
                                    ? Colors.white : Guidelines.secondaryColor,
                              ),
                              child: const Text("Favourites"),
                            ),
                            const SizedBox(width: 10),
                            TextButton(
                              onPressed: () {
                                setBuilderState(() {
                                  selector = RecentTripsSelector.longest;
                                });
                              },
                              style: TextButton.styleFrom(
                                backgroundColor: selector == RecentTripsSelector.longest
                                    ? Guidelines.tertiaryColor : Guidelines.primaryColor,
                                foregroundColor: selector == RecentTripsSelector.longest
                                    ? Colors.white : Guidelines.secondaryColor,
                              ),
                              child: const Text("Longest"),
                            )
                          ]
                        ),
                        const RecentTrips()
                      ]
                    );
                  }
                ),
                builder: (context, tripProvider, child) {
                  if (tripProvider.trips.isNotEmpty) {
                    return child!;
                  } else {
                    return const SizedBox();
                  }
                }
            ),
            Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Center(
                    child: SvgPicture.asset("assets/placeholder.svg", width: 275)
                )
            )
          ]
        )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async {
          String state = "cancel";

          if (context.read<TripProvider>().activeTrip != null) {
            state = await showDialog(
              context: context,
              builder: (context) {
                return DialogScaffold(
                  title: "You haven't finished your last trip!",
                  actions: [
                    TextButton(
                        style: DialogScaffold.actionStyle,
                        onPressed: () => context.pop("finish"),
                        child: const Text("Finish Active Trip")
                    ),
                    TextButton(
                        style: DialogScaffold.actionStyle,
                        onPressed: () => context.pop("deletion"),
                        child: const Text("Overwrite Active Trip")
                    )
                  ],
                  children: const [],
                );
              }
            ) ?? "cancel";
          } else {
            state = "deletion";
          }

          if (state == "deletion") {
            Provider.of<TripProvider>(
                context, listen: false
            ).beginTrip(Trip(
                title: "Current Trip",
                start: DateTime.now()
            ));

            context.pushNamed("navigation", extra: HandlingMethod.edit);
          } else if (state == "finish") {
            context.read<TripProvider>().selectedTrip = context.read<TripProvider>().activeTrip!;

            context.pushNamed("finish", extra: HandlingMethod.finish);
          }
        },
        isExtended: true,
        foregroundColor: Guidelines.secondaryColor,
        backgroundColor: Guidelines.primaryColor,
        elevation: 0,
        highlightElevation: 0,
        label: const Text("Begin A New Trip", style: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w600
        )),
        icon: const Icon(Icons.travel_explore_rounded),
      )
    );
  }
}

